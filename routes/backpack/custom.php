<?php
use Spatie\Permission\Traits\HasRoles;
// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => array_merge(
        (array) config('backpack.base.web_middleware', 'web'),
        (array) config('backpack.base.middleware_key', 'admin')
    ),
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    Route::crud('page-content', 'PageContentCrudController');
    Route::crud('page', 'PageCrudController');
    Route::crud('page-seo', 'PageSeoCrudController');
    Route::crud('menu', 'MenuCrudController');
    Route::crud('menu-element', 'MenuElementCrudController');
    Route::crud('category', 'CategoryCrudController');
    //Developer only
    Route::group(['middleware' => ['role:Developer']], function () {
        Route::crud('route', 'RouteCrudController');
        Route::crud('static-templates', 'StaticTemplatesCrudController');
    });
    Route::crud('vocabulary', 'VocabularyCrudController');
    Route::crud('slider', 'SliderCrudController');
    Route::get('charts/vocabulary', 'Charts\VocabularyChartController@response')->name('charts.vocabulary.index');
}); // this should be the absolute last line of this file