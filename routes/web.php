<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::redirect('/','/en');
Route::group(['prefix'=>'{language}'], function(){
    Route::get('/', function () {
        $this_menu = \App\Models\Menu::where('name','=','main-navigation')->first();
        $top_navigation = \App\Models\MenuElement::where('menu_id','=',$this_menu->id)->get();

        return view('home.main',compact('top_navigation'));
    })->middleware('check_locale');

    Route::get('page/{slug}', [\App\Http\Controllers\PageController::class, 'showPage'])->name('page')->middleware('check_locale');;
});

//Auth secured
Route::get('/profile', function () {
    return view('home.main');
})->middleware('backpack_auth');
