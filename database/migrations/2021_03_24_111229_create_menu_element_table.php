<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenuElementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_element', function (Blueprint $table) {
            $table->id();
            $table->integer('menu_id');
            $table->json('title')->nullable();
            $table->enum('type', ['static-page', 'route', 'url'])->default('url');
            $table->integer('page_id')->nullable();
            $table->integer('route_id')->nullable();
            $table->string('url')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_element');
    }
}
