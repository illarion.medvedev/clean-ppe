<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSliderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slider', function (Blueprint $table) {
            $table->id();
            $table->integer('page_id');
            $table->longText('image');
            $table->enum('type', ['static-page', 'route', 'url'])->default('url');
            $table->longText('template_id')->nullable();
            $table->longText('route_id')->nullable();
            $table->longText('url')->nullable();
            $table->enum('show', [0,1])->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('slider');
    }
}
