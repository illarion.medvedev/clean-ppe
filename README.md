## PPE Universe v2.0

## Main information
- Laravel Framework 8.33.1
- DB: Mysql
- Front-end compiler: Webpack laravel mix
- AWS s3 as image storage
- Compiler commands:
```
npm run watch
npm run dev
npm run prod
```

##Deployment
- Hosting: <a href="https://cloud.digitalocean.com/">DigitalOcean</a>
- Server management: <a href="https://forge.laravel.com/">Laravel Forge</a>
- Deployment: <a href="https://envoyer.io/">Envoyer</a>

## Libraries
- <a href="https://tailwindui.com/">Tailwind UI</a>
- <a href="https://tailwindcss.com/">Tailwind CSS</a>
- <a href="https://backpackforlaravel.com/">Backpack</a>  (Licensed)
- <a href="https://github.com/Laravel-Backpack/PermissionManager">Backpack\PermissionManager</a>

## Restrict access to user
- For routes:
```
->middleware('backpack_auth');
```
- Middleware path:
```
Http/Middleware/CheckIfAdmin.php
```
- For view:
```
@if(backpack_auth()->guest())
```
