<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\VocabularyRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Backpack\CRUD\app\Library\Widget;
/**
 * Class VocabularyCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class VocabularyCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CloneOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\BulkDeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\BulkCloneOperation;


    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Vocabulary::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/vocabulary');
        CRUD::setEntityNameStrings('phrase', 'Vocabulary');
        Widget::add([
            'type'       => 'chart',
            'controller' => \App\Http\Controllers\Admin\Charts\VocabularyChartController::class,
            'content' => [
                'header' => 'Latest activity',
                'body'   => 'Number of records created in relation to pages<br>',
            ],
            'wrapper' => ['class'=> 'row col-md-12'] ,
            'class'   => 'card col-md-12',
        ]);
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->addColumn([
            'name' => 'page',
            'type' => 'relationship',
            'label' => 'Page',
        ]);
        $this->crud->addColumn([
            'name' => 'name',
            'type' => 'text',
            'label' => 'Name',
        ]);
        $this->crud->addColumn([
            'name' => 'value',
            'type' => 'text',
            'label' => 'Value',
        ]);
        $this->crud->addColumn([
            'name' => 'created_at',
            'type' => 'datetime',
            'label' => 'Created at',
        ]);
        $this->crud->addColumn([
            'name' => 'updated_at',
            'type' => 'datetime',
            'label' => 'Updated at',
        ]);
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(VocabularyRequest::class);

        $this->crud->addField([
            'name' => 'page_id',
            'type' => 'relationship',
            'label' => 'Page',
        ]);
        $this->crud->addField([
            'name' => 'name',
            'type' => 'text',
            'label' => 'Name',
        ]);
        $this->crud->addField([
            'name' => 'value',
            'type' => 'text',
            'label' => 'Value',
        ]);
        $this->crud->addField([
            'name' => 'location',
            'type' => 'image',
            'label' => 'Image',
            'upload' => true,
            'crop' => true,
        ]);
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    protected function setupShowOperation()
    {
        $this->crud->set('show.setFromDb', false);
        $this->crud->addColumn([
            'name' => 'page',
            'type' => 'relationship',
            'label' => 'Page',
            'wrapper'   => [
                // 'element' => 'a', // the element will default to "a" so you can skip it here
                'href' => function ($crud, $column, $entry, $related_key) {
                    return backpack_url('page/'.$related_key.'/show');
                },
                // 'target' => '_blank',
                // 'class' => 'some-class',
            ],
        ]);
        $this->crud->addColumn([
            'name' => 'name',
            'type' => 'text',
            'label' => 'Name',
        ]);
        $this->crud->addColumn([
            'name' => 'value',
            'type' => 'text',
            'label' => 'Value',
        ]);
        $this->crud->addColumn([
            'name' => 'location',
            'type' => 'image',
            'label' => 'Image',
            'upload' => true,
            'crop' => true,
            'height' => '150px',
        ]);
        $this->crud->addColumn([
            'name' => 'created_at',
            'type' => 'datetime',
            'label' => 'Created at',
        ]);
        $this->crud->addColumn([
            'name' => 'updated_at',
            'type' => 'datetime',
            'label' => 'Updated at',
        ]);
    }
}
