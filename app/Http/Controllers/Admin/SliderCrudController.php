<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\SliderRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class SliderCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class SliderCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CloneOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\BulkDeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\BulkCloneOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Slider::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/slider');
        CRUD::setEntityNameStrings('slide', 'sliders');
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->addColumn([
            'name' => 'page',
            'type' => 'relationship',
            'label' => 'Page',
            'wrapper'   => [
                // 'element' => 'a', // the element will default to "a" so you can skip it here
                'href' => function ($crud, $column, $entry, $related_key) {
                    return backpack_url('page/'.$related_key.'/show');
                },
                // 'target' => '_blank',
                // 'class' => 'some-class',
            ],
        ]);
        $this->crud->addColumn([
            'name' => 'image',
            'type' => 'image',
            'label' => 'Image',
            'upload' => true,
            'crop' => true,
            'height' => '150px',
        ]);
        $this->crud->addColumn([
            'name' => 'type',
            'type' => 'text',
            'label' => 'URL type',
        ]);
        $this->crud->addColumn([
            'name' => 'show',
            'type' => 'radio',
            'label' => 'Status',
            'options'     => [
                0 => 'Draft',
                1 => 'Published'
            ]
        ]);
        $this->crud->addColumn([
            'name' => 'created_at',
            'type' => 'datetime',
            'label' => 'Created at',
        ]);
        $this->crud->addColumn([
            'name' => 'updated_at',
            'type' => 'datetime',
            'label' => 'Updated at',
        ]);
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(SliderRequest::class);

        $this->crud->addField([
            'name' => 'page_id',
            'type' => 'relationship',
            'label' => 'Page',
        ]);
        $this->crud->addField([
            'name' => 'image',
            'type' => 'image',
            'label' => 'Image',
            'upload' => true,
            'crop' => true,
        ]);
        $this->crud->addField([
            'name' => 'type',
            'label' => 'Link type',
            'type' => 'enum',
        ]);
        $this->crud->addField([
            'name' => 'template_id',
            'type' => 'relationship',
            'label' => 'Page',
        ]);
        $this->crud->addField([
            'name' => 'route_id',
            'type' => 'relationship',
            'label' => 'Route',
        ]);
        $this->crud->addField([
            'name' => 'url',
            'type' => 'text',
            'label' => 'URL',
        ]);
        $this->crud->addField([
            'name' => 'show',
            'type' => 'radio',
            'label' => 'Status',
            'options'     => [
                0 => 'Draft',
                1 => 'Published'
            ]
        ]);
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    protected function setupShowOperation()
    {
        $this->crud->set('show.setFromDb', false);
        $this->crud->addColumn([
            'name' => 'page',
            'type' => 'relationship',
            'label' => 'Page',
            'wrapper'   => [
                // 'element' => 'a', // the element will default to "a" so you can skip it here
                'href' => function ($crud, $column, $entry, $related_key) {
                    return backpack_url('page/'.$related_key.'/show');
                },
                // 'target' => '_blank',
                // 'class' => 'some-class',
            ],
        ]);
        $this->crud->addColumn([
            'name' => 'image',
            'type' => 'image',
            'label' => 'Image',
            'upload' => true,
            'crop' => true,
            'height' => '150px',
        ]);
        $this->crud->addColumn([
            'name' => 'type',
            'label' => 'Link type',
            'type' => 'enum',
        ]);
        $this->crud->addColumn([
            'name' => 'template_id',
            'type' => 'relationship',
            'label' => 'Page',
        ]);
        $this->crud->addColumn([
            'name' => 'route_id',
            'type' => 'relationship',
            'label' => 'Route',
        ]);
        $this->crud->addColumn([
            'name' => 'url',
            'type' => 'text',
            'label' => 'URL',
        ]);
        $this->crud->addColumn([
            'name' => 'show',
            'type' => 'radio',
            'label' => 'Status',
            'options'     => [
                0 => 'Draft',
                1 => 'Published'
            ]
        ]);
        $this->crud->addColumn([
            'name' => 'created_at',
            'type' => 'datetime',
            'label' => 'Created at',
        ]);
        $this->crud->addColumn([
            'name' => 'updated_at',
            'type' => 'datetime',
            'label' => 'Updated at',
        ]);
    }
}
