<?php

namespace App\Http\Controllers\Admin\Charts;

use App\Models\Page;
use App\Models\Vocabulary;
use Backpack\CRUD\app\Http\Controllers\ChartController;
use ConsoleTVs\Charts\Classes\Chartjs\Chart;
use Illuminate\Support\Facades\DB;

/**
 * Class VocabularyChartController
 * @package App\Http\Controllers\Admin\Charts
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class VocabularyChartController extends ChartController
{
    public function setup()
    {
        $this->chart = new Chart();

        // MANDATORY. Set the labels for the dataset points
        $labels = [];
        $this->chart->labels($labels);

        // RECOMMENDED. Set URL that the ChartJS library should call, to get its data using AJAX.
        $this->chart->load(backpack_url('charts/vocabulary'));

        // OPTIONAL
        $this->chart->minimalist(false);
        $this->chart->displayLegend(true);
    }

    /**
     * Respond to AJAX calls with all the chart data points.
     *
     * @return json
     */
     public function data()
     {
         function random_color_part() {
             return str_pad(mt_rand( 0, 255 ), 2, '0', STR_PAD_LEFT);
         }
         function random_color() {
             return 'rgba('.random_color_part().','.random_color_part().','.random_color_part();
         }
         $unique_variables = DB::table('vocabulary')
             ->join('page', 'vocabulary.page_id', '=', 'page.id')
             ->select('page.name','vocabulary.page_id', DB::raw('count(*) as total'))
             ->groupBy('page_id')
             ->get();

          //dd($unique_variables);
         foreach ($unique_variables as $variable) {
             $stable_color = random_color();
             $vocabulary[] = $variable->total;
             $this->chart->dataset($variable->name, 'bar', $vocabulary)
                 ->color($stable_color.")")
                 ->backgroundColor($stable_color.", 0.4)");
             unset($vocabulary);
         }
     }
}
