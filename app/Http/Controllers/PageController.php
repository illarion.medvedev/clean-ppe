<?php

namespace App\Http\Controllers;

use App\Models\PageContent;
use App\Models\PageSeo;
use App\Models\Page;

class PageController extends Controller
{
    public function showPage($lang, $slug){
        $page = PageContent::whereSlug($slug)->first();

        if (! $page){
            return view('errors.404');
        }
        $title = $page->title;
        return view('static-pages.default', compact('title', 'page'));
    }
}
